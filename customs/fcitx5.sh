if command -v fcitx5 >> /dev/null 2>&1; then
    if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
        export XMODIFIERS=@im=fcitx5
        if [ "$XDG_CURRENT_DESKTOP" = "GNOME" ]; then
            export GTK_IM_MODULE=fcitx5
            export QT_IM_MODULE=fcitx5
        fi
    elif [ "$XDG_SESSION_TYPE" = "x11" ]; then
        export GTK_IM_MODULE=fcitx5
        export QT_IM_MODULE=fcitx5
        export XMODIFIERS=@im=fcitx5
    fi
fi