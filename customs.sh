#!/bin/sh

if command -v doas >/dev/null 2>&1; then
    alias sudo='doas'
fi

get_harmony_os_sans(){
    wget https://developer.harmonyos.com/resource/image/design/res/download/general/HarmonyOS-Sans.zip
    sudo unzip HarmonyOS-Sans.zip -d /usr/local/share/fonts/HarmonyOS-Sans
}

apply_harmony_os_sans(){
    [ -d ~/.config/fontconfig ]||mkdir -p ~/.config/fontconfig
    cp customs/fontconfig/fonts.conf.xml ~/.config/fontconfig/fonts.conf
}

add_emoji_custom(){
    [ -d ~/.config/fontconfig ]||mkdir -p ~/.config/fontconfig
    [ -d ~/.config/fontconfig/conf.d ]||mkdir -p ~/.config/fontconfig/conf.d
    wget -O ~/.config/fontconfig/conf.d/75-noto-color-emoji.conf 'https://aur.archlinux.org/cgit/aur.git/plain/75-noto-color-emoji.conf?h=noto-color-emoji-fontconfig'
}

add_flatpak_fonts_hook(){
    echo 'if [ -d ~/.var/app ]; then
    for FlatpakAppDir in ~/.var/app/*; do
        if [ ! -L "$FlatpakAppDir"/config/fontconfig ]; then
            ln -s ~/.config/fontconfig "$FlatpakAppDir"/config/
        fi
    done
fi' | sudo tee /etc/profile.d/flatpak-fonts.sh
}

add_fcitx5_custom(){
    cat customs/fcitx5.sh | sudo tee /etc/profile.d/fcitx5.sh
    if [ -f ~/.config/gtk-3.0/settings.ini ]; then
        if [ "$(grep -c 'gtk-im-module' ~/.config/gtk-3.0/settings.ini)" -eq 0 ]; then
            echo 'gtk-im-module=fcitx5:im=fcitx5' >> ~/.config/gtk-3.0/settings.ini
        fi
    else
        echo '[Settings]
gtk-im-module=fcitx5:im=fcitx5' >> ~/.config/gtk-3.0/settings.ini
    fi
}

current_dir=$(pwd)
cd /tmp || exit
get_harmony_os_sans
apply_harmony_os_sans
add_emoji_custom
add_flatpak_fonts_hook
add_fcitx5_custom
cd "$current_dir" || exit
