#!/bin/bash

# shellcheck source=/dev/null

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
RESET=$(tput sgr0)

source "$(pwd)"/bin/ask_user.sh
source "$(pwd)"/bin/ask_partitions.sh
source "$(pwd)"/bin/ask_desktop.sh
source "$(pwd)"/bin/ask_locale_timezone.sh
source "$(pwd)"/bin/check_ucode.sh
source "$(pwd)"/bin/setup-base.sh

choose_ucode
ask_root_mountpoint
choose_root_mountpoint
ask_esp_mountpoint
choose_esp_mountpoint
ask_other_mountpoints
ask_swap
ask_locale
ask_timezone
ask_hostname
ask_desktop
choose_desktop
ask_sudo_user
while true; do
    if ! install_base; then
        echo "$RED""Installation base failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! gen_fstab; then
        echo "$RED""Generating fstab failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! install_systemd_boot; then
        echo "$RED""Installing systemd-boot failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! install_kernel; then
        echo "$RED""Installing kernel failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
if [ "$NO_DESKTOP" != "true" ];then
    while true; do
        if ! install_desktop; then
            echo "$RED""Installing desktop failed!""$RESET"
            read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
            if [ "$RETRY" = "n" ];then
                break
            fi
        else
            break
        fi
    done
    while true; do
        if ! apply_customs; then
            echo "$RED""Applying customs failed!""$RESET"
            read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
            if [ "$RETRY" = "n" ];then
                break
            fi
        else
            break
        fi
    done
fi
while true; do
    if ! add_sudo_user; then
        echo "$RED""Adding sudo user failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! set_locale; then
        echo "$RED""Setting locale failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! set_timezone; then
        echo "$RED""Setting timezone failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
while true; do
    if ! set_hostname; then
        echo "$RED""Setting hostname failed!""$RESET"
        read -p "$YELLOW""Do you want to retry? (y/n) ""$RESET" -r "RETRY"
        if [ "$RETRY" = "n" ];then
            break
        fi
    else
        break
    fi
done
echo "$GREEN""Installation finished!""$RESET"
