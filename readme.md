# A Arch Linux Installer by Bash

## usage

```sh
bash -c "$(curl -Ls https://gitlab.com/MarksonHo/arch-setup/-/raw/master/loader.sh)"
```

## Notice

Before running this script, make sure your partitions are ready to be used.