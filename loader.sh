#!/bin/bash

pacman-key --init && pacman-key --populate
pacman -Sy git --noconfirm
git clone https://gitlab.com/MarksonHo/arch-setup/
cd arch-setup || exit
./setup.sh