#!/bin/bash

function ask_root_mountpoint(){
    read -p "$YELLOW""Input your root partition: ""$RESET" -r "ROOT_PARTITION"
}

function choose_root_mountpoint(){
    while ! [ -b "$ROOT_PARTITION" ]; do
        echo "$RED""The root partition you typed in is not available!""$RESET"
        ask_root_mountpoint
    done
    read -p "$YELLOW""Do you want to format your root partition? (y/n) ""$RESET" -r "FORMAT_ROOT"
    if [ "$FORMAT_ROOT" = "y" ];then
        echo "choose filesystem type, such as ext4, btrfs, xfs, f2fs"
        read -p "$YELLOW""Input your filesystem type: ""$RESET" -r "FILESYSTEM_ROOT"
        while [ -n "$FILESYSTEM_ROOT" ]; do
            if command -v mkfs."$FILESYSTEM_ROOT" >/dev/null 2>&1; then
                mkfs."$FILESYSTEM_ROOT" "$ROOT_PARTITION"
                break
            else
                echo "$RED""Filesystem you chosen not found!""$RESET"
                echo "$RED""Please choose another one!""$RESET"
                read -p "$YELLOW""Input your filesystem type: ""$RESET" -r "FILESYSTEM_ROOT"
            fi
        done        
    fi
    mount "$ROOT_PARTITION" /mnt
}

function ask_esp_mountpoint(){
    read -p "$YELLOW""Input your EFI partition: ""$RESET" -r "ESP"
}

function choose_esp_mountpoint(){
    while ! [ -b "$ESP" ]; do
        echo "$RED""The EFI partition you typed in is not available!""$RESET"
        ask_esp_mountpoint
    done
    read -p "$YELLOW""Do you want to format your EFI partition? (y/n) ""$RESET" -r "FORMAT_ESP"
    if [ "$FORMAT_ESP" = "y" ];then
        mkfs.vfat "$ESP"
    fi
    mkdir -p /mnt/boot/efi
    mount "$ESP" /mnt/boot/efi
}

function ask_other_mountpoints(){
    while true; do
        read -p "$YELLOW""Do you want to mount other partitions? (y/n) ""$RESET" -r "MOUNT_OTHERS"
        if [ "$MOUNT_OTHERS" = "y" ];then
            read -p "$YELLOW""Input your partition: ""$RESET" -r "OTHER_PARTITION"
            while ! [ -b "$OTHER_PARTITION" ]; do
                echo "$RED""The partition you typed in is not available!""$RESET"
                read -p "$YELLOW""Input your partition: ""$RESET" -r "OTHER_PARTITION"
            done
            read -p "$YELLOW""Input your mountpoint: ""$RESET" -r "OTHER_MOUNTPOINT"
            read -p "$YELLOW""Do you want to format your partition? (y/n) ""$RESET" -r "FORMAT_OTHER"
            if [ "$FORMAT_OTHER" = "y" ];then
                echo "choose filesystem type, such as ext4, btrfs, xfs, f2fs"
                read -p "$YELLOW""Input your filesystem type: ""$RESET" -r "FILESYSTEM_OTHER"
                while [ -n "$FILESYSTEM_OTHER" ]; do
                    if command -v mkfs."$FILESYSTEM_OTHER" >/dev/null 2>&1; then
                        mkfs."$FILESYSTEM_OTHER" "$OTHER_PARTITION"
                        break
                    else
                        echo "$RED""Filesystem you chosen not found!""$RESET"
                        echo "$RED""Please choose another one!""$RESET"
                        read -p "$YELLOW""Input your filesystem type: ""$RESET" -r "FILESYSTEM_OTHER"
                    fi
                done
            fi
            mkdir -p /mnt/"$OTHER_MOUNTPOINT"
            mount "$OTHER_PARTITION" /mnt/"$OTHER_MOUNTPOINT"
        else
            break
        fi
    done
}

function ask_swap(){
    read -p "$YELLOW""Do you want to create swap partition? (y/n) ""$RESET" -r "CREATE_SWAP"
    if [ "$CREATE_SWAP" = "y" ];then
        read -p "$YELLOW""Input your swap partition: ""$RESET" -r "SWAP_PARTITION"
        while ! [ -b "$SWAP_PARTITION" ]; do
            echo "$RED""The swap partition you typed in is not available!""$RESET"
            read -p "$YELLOW""Input your swap partition: ""$RESET" -r "SWAP_PARTITION"
        done
        read -p "$YELLOW""Do you want to format your swap partition? (y/n) ""$RESET" -r "FORMAT_SWAP"
        if [ "$FORMAT_SWAP" = "y" ];then
            mkswap "$SWAP_PARTITION"
        fi
        swapon "$SWAP_PARTITION"
    fi
}