#!/bin/bash

function install_desktop() {
    pacstrap /mnt plasma kde-system okular spectacle arianna gwenview ark kate konsole kwalletmanager kweather print-manager system-config-printer cups-pk-helper yakuake plasma-wayland-session firefox pipewire-pulse pipewire-jack noto-fonts-cjk noto-fonts-emoji noto-fonts-extra noto-fonts
    arch-chroot /mnt /bin/bash -c "systemctl enable sddm.service"
    arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager.service"
    arch-chroot /mnt /bin/bash -c "systemctl enable bluetooth.service"
}

function apply_customs() {
    echo 'unset GDK_BACKEND
/usr/bin/xdg-open "$@"' > /mnt/usr/local/bin/xdg-open

    mkdir -p /mnt/etc/sddm.conf.d
    echo '[General]
DisplayServer=wayland
GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell
[Wayland]
CompositorCommand=kwin_wayland --no-lockscreen --no-global-shortcuts' > /mnt/etc/sddm.conf.d/wayland.conf
}