#!/bin/bash

function ask_locale(){
    while true; do
        echo "$YELLOW""Input your locale, such as en_US, zh_CN or de_DE.""$RESET"
        read -p "$YELLOW""Input locale: ""$RESET" -r "USER_LOCALE"
        check_locale=$(grep "$USER_LOCALE" < /etc/locale.gen)
        if [ -n "$check_locale" ]; then
            echo "$GREEN""Your locale is""$RESET ""$USER_LOCALE"
            break
        else
            echo "$RED""Your locale is invaild, input again!""$RESET"
        fi
    done
}

function ask_timezone(){
    while true; do
        echo "$YELLOW""Input your timezone, such as Asia/Shanghai, America/New_York.""$RESET"
        read -p "$YELLOW""Input timezone: ""$RESET" -r "USER_TIMEZONE"
        if [ -f /usr/share/zoneinfo/"$USER_TIMEZONE" ];then
            echo "$GREEN""Your timezone is""$RESET ""$USER_TIMEZONE"
            break
        else
            echo "$RED""Your timezone is invaild, input again!""$RESET"
        fi
    done
}

function ask_hostname(){
    read -p "$YELLOW""Input your hostname: ""$RESET" -r "HOSTNAME"
    if [ -z "$HOSTNAME" ];then
        echo "$YELLOW""Hostname you typed in is empty, use archlinux as hostname instead.""$RESET"
        HOSTNAME="archlinux"
    fi
}

function set_locale()(
    real_locale=$(grep UTF-8  < /mnt/etc/locale.gen | grep "$USER_LOCALE" | sed 's|#||g')
    sed -i "s|#$real_locale|$real_locale|" /mnt/etc/locale.gen
    echo "LANG=$USER_LOCALE.UTF-8" >> /mnt/etc/locale.conf
    arch-chroot /mnt /bin/bash -c "locale-gen"
)

function set_timezone(){
    arch-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo/\"$USER_TIMEZONE\" /etc/localtime"
    arch-chroot /mnt /bin/bash -c "hwclock --systohc"
}

function set_hostname(){
    echo "$HOSTNAME" > /mnt/etc/hostname
}