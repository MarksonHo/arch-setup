#!/bin/bash

function ask_desktop() {
    read -p "$YELLOW""Do you want to install a desktop? (y/n) ""$RESET" -r "INSTALL_DESKTOP"
    if [ "$INSTALL_DESKTOP" = "y" ];then
        echo "$GREEN""Choose a desktop""$RESET"
        echo "$GREEN""1. Gnome""$RESET"
        echo "$GREEN""2. KDE Plasma""$RESET"
        read -r -p "$YELLOW""input your choice: ""$RESET" -r "INSTALL_DESKTOP"
    else
        NO_DESKTOP="true"
    fi
}

function choose_desktop() {
    if [ "$NO_DESKTOP" = "true" ];then
        return
    else
        while true; do
            if [ "$INSTALL_DESKTOP" = "1" ];then
                source $(pwd)/bin/setup-gnome.sh
                break
            elif [ "$INSTALL_DESKTOP" = "2" ];then
                source $(pwd)/bin/setup-plasma.sh
                break
            else
                echo "$RED""Please input a valid number!""$RESET"
                read -r -p "$YELLOW""input your choice: ""$RESET" -r "INSTALL_DESKTOP"
            fi
        done
    fi
}