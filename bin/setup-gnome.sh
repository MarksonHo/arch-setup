#!/bin/bash

function install_desktop() {
    pacstrap /mnt gnome gnome-terminal gnome-tweaks gnome-notes noto-fonts-cjk noto-fonts-emoji noto-fonts-extra noto-fonts
    arch-chroot /mnt /bin/bash -c "systemctl enable gdm.service"
    arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager.service"
    arch-chroot /mnt /bin/bash -c "systemctl enable bluetooth.service"
}

function apply_customs() {
    echo 'unset GDK_BACKEND 
/usr/bin/xdg-open "$@"' > /mnt/usr/local/bin/xdg-open
}