#!/bin/bash

function ask_sudo_user(){
    while true; do
        echo "$YELLOW""Now you should add user with sudo access, an username should always be lowercase:""$RESET"
        read -p "$YELLOW""Input your username: ""$RESET" -r "USERNAME"
        check_username=$(grep -E "^$USERNAME" /etc/passwd)
        if echo "$USERNAME" | grep -q "^[a-z]*$"; then
            if [ -z "$check_username" ]; then
                echo "$GREEN""Your username is""$RESET ""$USERNAME"
                break
            else
                echo "$RED""Your username is already exist, or you have not input your username!""$RESET"
            fi
        else
            echo "$RED""Username should always be lowercase, input again!""$RESET"
        fi
    done
    while true; do
        read -p "$YELLOW""Input your password: ""$RESET" -s -r "PASSWORD"; printf "\n"
        read -p "$YELLOW""Input your password again: ""$RESET" -s -r "PASSWORD_AGAIN"; printf "\n"
        if [ "$PASSWORD" == "$PASSWORD_AGAIN" ]; then
            echo "$GREEN""Set password successfully""$RESET"
            break
        else
            echo "$RED""The password you input firstly is NOT match the password you input secondly!""$RESET"
        fi
    done
}

function add_sudo_user(){
    arch-chroot /mnt /bin/bash -c "useradd -m -G wheel -s /bin/zsh $USERNAME"
    arch-chroot /mnt /bin/bash -c "echo $USERNAME:$PASSWORD | chpasswd"
    sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /mnt/etc/sudoers
}