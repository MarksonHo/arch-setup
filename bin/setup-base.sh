#!/bin/bash

function install_base(){
    pacstrap -K /mnt base base-devel linux-firmware dnsutils usbutils libva-utils "$ucode" zsh unrar p7zip unzip sudo nano networkmanager
    arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"
    pacstrap /mnt libva-mesa-driver intel-media-driver libva-intel-driver vulkan-intel vulkan-radeon vulkan-mesa-layers
}

function gen_fstab(){
    genfstab -U /mnt | tee /mnt/etc/fstab
}

function install_systemd_boot(){
    arch-chroot /mnt /bin/bash -c "bootctl install --esp-path=/boot/efi"
    pacstrap /mnt dracut plymouth
    mkdir -p /mnt/etc/dracut.conf.d
    root_partition_part_uuid="$(lsblk "$ROOT_PARTITION" -no PARTUUID)"
    echo "kernel_cmdline=""\"root=PARTUUID=$root_partition_part_uuid rw rootfstype=$FILESYSTEM_ROOT splash quiet\"" > /mnt/etc/dracut.conf.d/cmdline.conf
    echo "compress=zstd" > /mnt/etc/dracut.conf.d/compress.conf
    cp dracut/bin/dracut-install.sh /mnt/usr/local/bin/dracut-install.sh
    cp dracut/bin/dracut-remove.sh /mnt/usr/local/bin/dracut-remove.sh
    chmod +x /mnt/usr/local/bin/dracut-install.sh
    chmod +x /mnt/usr/local/bin/dracut-remove.sh
    mkdir /mnt/etc/pacman.d/hooks
    cp dracut/hooks/dracut-install.hook /mnt/etc/pacman.d/hooks/60-dracut-install.hook
    cp dracut/hooks/dracut-remove.hook /mnt/etc/pacman.d/hooks/90-dracut-remove.hook
}

function install_kernel(){
    arch-chroot /mnt/ /bin/bash -c "pacman -S linux linux-headers linux-firmware --noconfirm"
}