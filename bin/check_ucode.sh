#!/bin/bash

function choose_ucode(){
    if [ -n "$(cat /proc/cpuinfo | grep Intel)" ];then
        ucode="intel-ucode"
    elif [ -n "$(cat /proc/cpuinfo | grep AMD)" ];then
        ucode="amd-ucode"
    fi
}